package me.mathdu07.vocabulary;

import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.opencsv.CSVReader;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final int ACTIVITY_IMPORT_LIST = 1;
    private static final int ACTIVITY_CHOOSE_OPEN_FILE = 3;

    public static final String RANDOM = "vocabulary-random";
    public static final String INVERT = "vocabulary-invert";

    public static final Random RANDOM_GENERATOR = new Random();

    private boolean random = false, invert = false;
    private static VocDbHelper dbHelper;
    private static VocData data;
    private Fragment currentFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new VocDbHelper(getApplicationContext());

        if (savedInstanceState != null) {
            this.random = savedInstanceState.getBoolean(RANDOM);
            this.invert = savedInstanceState.getBoolean(INVERT);
        } else {
            data = new VocData();
            data.load();
            random = false;
            invert = false;
        }

        setContentView(R.layout.activity_main);
        currentFragment = new MainActivityFragment();
        getIntent().putExtra(RANDOM, random);
        getIntent().putExtra(INVERT, invert);
        currentFragment.setArguments(getIntent().getExtras());
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, currentFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem randomItem = menu.findItem(R.id.action_random);
        randomItem.setChecked(random);

        MenuItem invertItem = menu.findItem(R.id.action_invert);
        invertItem.setChecked(invert);

        super.onCreateOptionsMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_random) {
            item.setChecked(!item.isChecked());
            random = item.isChecked();
            getIntent().putExtra(RANDOM, random);
            currentFragment.setArguments(getIntent().getExtras());

            return true;
        } else if (id == R.id.action_invert) {
            item.setChecked(!item.isChecked());
            invert = item.isChecked();
            getIntent().putExtra(INVERT, invert);
            currentFragment.setArguments(getIntent().getExtras());

            return true;
        } else if (id == R.id.action_import) {
            Intent intent;
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("text/*");
            startActivityForResult(intent, ACTIVITY_CHOOSE_OPEN_FILE);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK)
            return;

        switch (requestCode) {
            case ACTIVITY_CHOOSE_OPEN_FILE: {
                Uri uri = data.getData();
                String path = uri.getPath();

                Cursor c = getContentResolver().query(uri, null, null, null, null);

                try {
                    if (c != null && c.moveToFirst()) {
                        // Assuming DISPLAY_NAME returns file's name
                        String displayName = c.getString(c.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        if (!displayName.endsWith(".csv")) {
                            Log.i("Import", "Tried to import non-csv file : " + displayName);
                            Snackbar.make(findViewById(R.id.fragment_container), R.string.error_wrong_file_type, Snackbar.LENGTH_LONG)
                                    .show();
                        } else {
                            new ReadVocFile().execute(uri);
                        }

                    }
                } finally {
                    c.close();
                }
                break;
            }
            case ACTIVITY_IMPORT_LIST: {
                if (currentFragment instanceof MainActivityFragment) {
                    ((MainActivityFragment) currentFragment).updateUI();
                }
                break;
            }
            default:
                Log.w("onActivityResult", "Unknown request code : " + requestCode);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(RANDOM, random);
        outState.putBoolean(INVERT, invert);
    }

    private class ReadVocFile extends AsyncTask<Uri, Integer, List<String[]>> {


        @Override
        protected List<String[]> doInBackground(Uri... uris) {
            Uri uri = uris[0];
            List<String[]> words = new ArrayList<>();

            try {
                InputStream is = getContentResolver().openInputStream(uri);
                BufferedInputStream bis = new BufferedInputStream(is);
                Reader r = new InputStreamReader(bis);

                CSVReader reader = new CSVReader(r);
                String[] line = null;

                while ((line = reader.readNext()) != null) {
                    if (isCancelled() || line.length != 2) {
                        words = null;
                        break;
                    }

                    words.add(line);
                }


                reader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                words = null;
            } catch (IOException e) {
                e.printStackTrace();
                words = null;
            }


            return words;
        }

        @Override
        protected void onPostExecute(List<String[]> result) {


            if (result != null) {
                String[] words1, words2;
                int size = result.size();
                words1 = new String[size];
                words2 = new String[size];

                for (int i = 0; i < size; i++) {
                    String[] words = result.get(i);
                    words1[i] = words[0];
                    words2[i] = words[1];
                }

                Intent intent = new Intent(getApplicationContext(), ImportListActivity.class);
                intent.putExtra(ImportListActivity.WORDS_LANGUAGE_1, words1);
                intent.putExtra(ImportListActivity.WORDS_LANGUAGE_2, words2);

                startActivityForResult(intent, ACTIVITY_IMPORT_LIST);
            } else {
                Snackbar.make(findViewById(R.id.fragment_container), R.string.error_impossible_read, Snackbar.LENGTH_LONG)
                        .show();
            }

        }
    }

    public static VocDbHelper getDbHelper() {
        return dbHelper;
    }

    public static VocData getData() {  return data;  }
}
