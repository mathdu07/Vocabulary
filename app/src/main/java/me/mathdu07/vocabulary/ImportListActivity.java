package me.mathdu07.vocabulary;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ImportListActivity extends AppCompatActivity {

    public static final String WORDS_LANGUAGE_1 = "vocabulary-import-words-1";
    public static final String WORDS_LANGUAGE_2 = "vocabulary-import-words-2";
    public static final String IMPORTING = "vocabulary-import-importing";

    private String[] words1, words2;
    private boolean importing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_list);

        if (savedInstanceState != null)
        {
            words1 = savedInstanceState.getStringArray(WORDS_LANGUAGE_1);
            words2 = savedInstanceState.getStringArray(WORDS_LANGUAGE_2);
            importing = savedInstanceState.getBoolean(IMPORTING);
        }
        else
        {
            Intent intent = getIntent();

            words1 = intent.getStringArrayExtra(WORDS_LANGUAGE_1);
            words2 = intent.getStringArrayExtra(WORDS_LANGUAGE_2);
        }

        TextView wordsCount = (TextView) findViewById(R.id.wordsCountText);
        wordsCount.setText(Integer.toString(words1.length));

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putStringArray(WORDS_LANGUAGE_1, words1);
        outState.putStringArray(WORDS_LANGUAGE_2, words2);
        outState.putBoolean(IMPORTING, importing);
    }

    public void doImportation(View view)
    {
        String name = ((EditText) findViewById(R.id.titleEdit)).getText().toString();
        String lang1 = ((EditText) findViewById(R.id.language1Edit)).getText().toString();
        String lang2 = ((EditText) findViewById(R.id.language2Edit)).getText().toString();

        String tableName = VocData.nameToTable(name);
        VocData data = MainActivity.getData();

        if (data.exist(tableName))
        {
            Snackbar.make(getCurrentFocus(), R.string.error_already_exists, Snackbar.LENGTH_LONG)
                    .show();
        }
        else
        {
            VocList list = data.createVocList(tableName, name, lang1, lang2);
            importing = true;
            ((Button) findViewById(R.id.importButton)).setEnabled(false);

            new AddWords().execute(list);
        }
    }

    private class AddWords extends AsyncTask<VocList, Integer, VocList>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ProgressBar bar = (ProgressBar) findViewById(R.id.progressBar);
            bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected VocList doInBackground(VocList... vocLists) {
            VocList list = vocLists[0];

            list.addWords(words1, words2);
            list.saveTable();

            return list;
        }

        @Override
        protected void onPostExecute(VocList vocList) {
            super.onPostExecute(vocList);

            ProgressBar bar = (ProgressBar) findViewById(R.id.progressBar);
            bar.setVisibility(View.INVISIBLE);

            setResult(RESULT_OK);
            finish();
        }
    }
}
