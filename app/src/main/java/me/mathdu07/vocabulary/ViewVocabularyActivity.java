package me.mathdu07.vocabulary;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ViewVocabularyActivity extends AppCompatActivity {

    public static final String LIST = "vocabulary-list";
    public static final String INDEX = "vocabulary-view-index";
    public static final String REVEALED = "vocabulary-view-revealed";
    public static final String FIRST_WORDS = "vocabulary-view-words-1";
    public static final String SECOND_WORDS = "vocabulary-view-words-2";

    private VocList list;
    private int index = 0;
    private boolean revealed = false;
    private TextView firstWord, secondWord;

    private String[] firstWords, secondWords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
        {
            this.list = (VocList) savedInstanceState.getSerializable(LIST);
            this.index = savedInstanceState.getInt(INDEX);
            this.revealed = savedInstanceState.getBoolean(REVEALED);
            this.firstWords = savedInstanceState.getStringArray(FIRST_WORDS);
            this.secondWords = savedInstanceState.getStringArray(SECOND_WORDS);
        }
        else
        {
            Intent intent = getIntent();
            list = (VocList) intent.getExtras().getSerializable(LIST);
            boolean random = intent.getExtras().getBoolean(MainActivity.RANDOM);
            boolean invert = intent.getExtras().getBoolean(MainActivity.INVERT);

            constructWordArray(random, invert);
        }

        setTitle(list.getName());
        setContentView(R.layout.activity_view_vocabulary);

        firstWord = (TextView) findViewById(R.id.firstWord);
        secondWord = (TextView) findViewById(R.id.secondWord);
        firstWord.setVisibility(View.VISIBLE);
        secondWord.setVisibility(revealed ? View.VISIBLE : View.INVISIBLE);
        // if second word is revealed, index has been incremented
        firstWord.setText(firstWords[index - (revealed ? 1 : 0)]);
        secondWord.setText(secondWords[index - (revealed ? 1 : 0)]);
    }

    private void constructWordArray(boolean random, boolean invert)
    {
        int size = list.getWordsCount();
        firstWords = new String[size];
        secondWords = new String[size];

        if (random)
        {
            Random r = MainActivity.RANDOM_GENERATOR;
            List<Integer> remainingIndexes = new ArrayList<>();
            for (int i = 0; i < size; i++)
                remainingIndexes.add(i);

            int n = size;
            for (int i = 0; i < size; i++)
            {
                int index = r.nextInt(n);

                firstWords[i] = list.getWordFL(remainingIndexes.get(index));
                secondWords[i] = list.getWorldSL(remainingIndexes.get(index));

                remainingIndexes.remove(index);
                n--;
            }
        }
        else
        {
            for (int i = 0; i < size; i++)
            {
                firstWords[i] = list.getWordFL(i);
                secondWords[i] = list.getWorldSL(i);
            }
        }

        if (invert)
        {
            String[] aux = firstWords;
            firstWords = secondWords;
            secondWords = aux;
        }
    }

    public void onClick(View view)
    {
        if (!revealed)
        {
            secondWord.setVisibility(View.VISIBLE);
            revealed = true;
            index++;
        }
        else
        {
            if (index >= list.getWordsCount())
            {
                /*Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);*/
                finish();
                return;
            }

            secondWord.setVisibility(View.INVISIBLE);

            firstWord.setText(firstWords[index]);
            secondWord.setText(secondWords[index]);

            revealed = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(LIST, list);
        outState.putInt(INDEX, index);
        outState.putBoolean(REVEALED, revealed);
        outState.putStringArray(FIRST_WORDS, firstWords);
        outState.putStringArray(SECOND_WORDS, secondWords);
    }
}
