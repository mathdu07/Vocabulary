package me.mathdu07.vocabulary;

import java.util.HashMap;
import java.util.Map;

public class VocData
{
    private Map<String, VocList> vocMap = new HashMap<>();

    public VocData()
    {

    }

    public void load()
    {
        VocDbHelper dbHelper = MainActivity.getDbHelper();
        String[] tables = dbHelper.getVocListTables();

        for (String table : tables)
        {
            VocList list = dbHelper.loadVocList(table);
            vocMap.put(table, list);
            /*
            Log.d("SQL", "List name : " + list.getName());
            Log.d("SQL", "List' 1st lang : " + list.getFirstLanguage());
            Log.d("SQL", "List' 2nd lang : " + list.getSecondLanguage());
            */
        }
    }

    public String[] getListsName()
    {
        return vocMap.keySet().toArray(new String[vocMap.size()]);
    }

    public String[] getListsDisplayName()
    {
        String[] names = getListsName();
        String[] displayNames = new String[names.length];

        for (int i = 0; i < displayNames.length; i++)
        {
            VocList list = getVocList(names[i]);
            displayNames[i] = list.getName();
        }

        return displayNames;
    }

    public VocList getVocList(String name)
    {
        return vocMap.get(name);
    }

    public VocList getVocListAndLoad(String name)
    {
        VocList list = vocMap.get(name);
        list.readTable();

        return list;
    }

    public boolean exist(String tableName)
    {
        return vocMap.containsKey(tableName);
    }

    public VocList createVocList(String tableName, String name, String firstLanguage, String secondLanguage)
    {
        VocDbHelper dbHelper = MainActivity.getDbHelper();
        VocList list = dbHelper.createVocList(tableName, name, firstLanguage, secondLanguage);

        vocMap.put(tableName, list);

        return list;
    }

    public static String nameToTable(String name)
    {
        String tableName = name.trim().toLowerCase();
        tableName = tableName.replace(",", "").replace("\"", "").replace("\'", "").replace("`", "");
        tableName = tableName.replace(";", "").replace(":", "").replace("\n", "").replace("-", "_");
        tableName = tableName.replace(" ", "_");

        return tableName;
    }
}
