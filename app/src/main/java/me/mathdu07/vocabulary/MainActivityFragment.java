package me.mathdu07.vocabulary;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;


public class MainActivityFragment extends ListFragment {

    private boolean random, invert;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle bundle) {
        super.onCreate(bundle);

        if (bundle != null) {
            this.random = bundle.getBoolean(MainActivity.RANDOM);
            this.invert = bundle.getBoolean(MainActivity.INVERT);
        }

        View view = super.onCreateView(inflater, container, bundle);
        setListAdapter(new VocListAdapter(MainActivity.getData(), getActivity()));

        return view;
    }

    public void updateUI()
    {
        VocListAdapter adapter = new VocListAdapter(MainActivity.getData(), getActivity());
        adapter.notifyDataSetChanged();
        setListAdapter(adapter);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(MainActivity.RANDOM, random);
        outState.putBoolean(MainActivity.INVERT, invert);
    }


    @Override
    public void setArguments(Bundle bundle)
    {
        this.random = bundle.getBoolean(MainActivity.RANDOM);
        this.invert = bundle.getBoolean(MainActivity.INVERT);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int i, long l) {
            VocListAdapter adapter = (VocListAdapter) getListAdapter();

            String id = adapter.getKey(i);
            VocList list = MainActivity.getData().getVocList(id);
            list.readTable();

            Intent intent = new Intent(getActivity(), ViewVocabularyActivity.class);
            intent.putExtra(ViewVocabularyActivity.LIST, list);
            intent.putExtra(MainActivity.RANDOM, random);
            intent.putExtra(MainActivity.INVERT, invert);
            startActivity(intent);
    }
}
