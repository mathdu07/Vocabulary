package me.mathdu07.vocabulary;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VocListAdapter extends BaseAdapter
{
    private LayoutInflater inflater;
    private List<String> keyList;
    private VocData data;

    public VocListAdapter(VocData data, Activity activity)
    {
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.keyList = new ArrayList<String>();
        this.data = data;

        String[] keys = data.getListsName();
        // TODO Have voc list already sorted when gathered from SQLite
        Arrays.sort(keys);
        for (String key : keys)
            keyList.add(key);


    }

    @Override
    public int getCount() {
        return keyList.size();
    }

    @Override
    public Object getItem(int position) {
        return data.getVocList(keyList.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (convertView == null) {
            view = inflater.inflate(R.layout.voc_list_item, parent, false);
        } else {
            view = convertView;
        }

        TextView title = (TextView) view.findViewById(R.id.vocListItemTitle);
        TextView firstLanguage = (TextView) view.findViewById(R.id.vocListItemLanguage1);
        TextView secondLanguage = (TextView) view.findViewById(R.id.vocListItemLanguage2);

        VocList vocList = (VocList) getItem(position);
        title.setText(vocList.getName());
        firstLanguage.setText(vocList.getFirstLanguage());
        secondLanguage.setText(vocList.getSecondLanguage());

        return view;
    }

    public String getKey(int index)
    {
        return keyList.get(index);
    }
}
