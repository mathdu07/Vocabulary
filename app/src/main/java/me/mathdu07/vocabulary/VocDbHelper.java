package me.mathdu07.vocabulary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VocDbHelper extends SQLiteOpenHelper implements Serializable
{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Vocabulary.db";


    public VocDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(IndexTable.CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public String[] getVocListTables()
    {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(
                "\"" + IndexTable.TABLE_NAME + "\"",
                new String[]{IndexTable.COLUMN_NAME_TABLE_NAME},
                null,
                null,
                null,
                null,
                null
        );

        List<String> tables = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                tables.add(cursor.getString(cursor.getColumnIndex(IndexTable.COLUMN_NAME_TABLE_NAME)));
            } while (cursor.moveToNext());
        }

        cursor.close();

        return tables.toArray(new String[tables.size()]);
    }

    public Cursor getRowByTableName(String table, SQLiteDatabase db)
    {
        return db.query(
                "\"" + IndexTable.TABLE_NAME + "\"", // Index is a SQLite keyword, so it needs to be quoted
                new String[]{IndexTable.COLUMN_NAME_NAME, IndexTable.COLUMN_NAME_LANGUAGE_1,
                        IndexTable.COLUMN_NAME_LANGUAGE_2},
                IndexTable.COLUMN_NAME_TABLE_NAME + " = \"" + table + "\"",
                null,
                null,
                null,
                ""
        );
    }

    /**
     * Creates a new Vocabulary List and it's table
     * @param tableName
     * @param name
     * @param language1
     * @param language2
     * @return created voc list
     */
    public VocList createVocList(String tableName, String name, String language1, String language2)
    {
        ContentValues values = new ContentValues();
        values.put(IndexTable.COLUMN_NAME_TABLE_NAME, tableName);
        values.put(IndexTable.COLUMN_NAME_NAME, name);
        values.put(IndexTable.COLUMN_NAME_LANGUAGE_1, language1);
        values.put(IndexTable.COLUMN_NAME_LANGUAGE_2, language2);

        getWritableDatabase().insert("\"" + IndexTable.TABLE_NAME + "\"", null, values);

        VocList list = new VocList(tableName);
        list.createTable();

        return list;
    }

    /**
     * Loads an existing Vocabulary List.
     * It only loads metadatas of the list (ie name, languages ...),
     * not data itself (ie not list's words)
     * @param tableName
     * @return "semi-"loaded list
     */
    public VocList loadVocList(String tableName)
    {
        VocList list = new VocList(tableName);

        return list;
    }

    public class IndexTable implements BaseColumns
    {
        public static final String TABLE_NAME = "index";
        public static final String COLUMN_NAME_TABLE_NAME = "table_name";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LANGUAGE_1 = "language_1";
        public static final String COLUMN_NAME_LANGUAGE_2 = "language_2";

        public static final String TYPE_TEXT = "TEXT";

        public static final String CREATE_QUERY = "CREATE TABLE \"" + TABLE_NAME
                + "\" (" + COLUMN_NAME_TABLE_NAME + " " + TYPE_TEXT + " PRIMARY KEY, "
                + COLUMN_NAME_NAME + " " + TYPE_TEXT + ", "
                + COLUMN_NAME_LANGUAGE_1 + " " + TYPE_TEXT + ", "
                + COLUMN_NAME_LANGUAGE_2 + " " + TYPE_TEXT + ")";


    }
}
