package me.mathdu07.vocabulary;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VocList implements Serializable {

    private String tableName, name;
    private String language1Name, language2Name;

    private List<String> words1;
    private List<String> words2;

    private boolean loaded = false;

    public VocList(String name)
    {
        VocDbHelper dbHelper = MainActivity.getDbHelper();
        this.tableName = name;

        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = dbHelper.getRowByTableName(tableName, db);
        c.moveToFirst();

        this.name = c.getString(c.getColumnIndex(VocDbHelper.IndexTable.COLUMN_NAME_NAME));
        language1Name = c.getString(c.getColumnIndex(VocDbHelper.IndexTable.COLUMN_NAME_LANGUAGE_1));
        language2Name = c.getString(c.getColumnIndex(VocDbHelper.IndexTable.COLUMN_NAME_LANGUAGE_2));
        c.close();

        words1 = new ArrayList<>();
        words2 = new ArrayList<>();
    }

    /**
     * Adds a word and its translation to the list
     * @param word1 word in first language
     * @param word2 word in second language
     */
    public void addWord(String word1, String word2)
    {
        this.words1.add(word1);
        this.words2.add(word2);
    }

    /**
     * Adds words and their translations
     * @param words1
     * @param words2
     */
    public void addWords(String[] words1, String[] words2)
    {
        if (words1.length != words2.length)
            throw new IllegalArgumentException("There must have the same amount of words for each language !");

        for (int i = 0; i < words1.length; i++)
        {
            addWord(words1[i], words2[i]);
        }
    }

    /**
     *
     * @param index
     * @return the word at the given index from the first language
     */
    public String getWordFL(int index)
    {
        return words1.get(index);
    }

    /**
     *
     * @param index
     * @return the word at the given index from the second language
     */
    public String getWorldSL(int index)
    {
        return words2.get(index);
    }

    /**
     *
     * @return amount of words in the list
     */
    public int getWordsCount()
    {
        return words1.size();
    }

    /**
     * Removes word and its translation by its index
     * @param index
     */
    public void removeWord(int index)
    {
        words1.remove(index);
        words2.remove(index);
    }

    /**
     * Removes all words and translations
     */
    public void clear()
    {
        words1.clear();
        words2.clear();
    }

    public void createTable()
    {
        VocDbHelper dbHelper = MainActivity.getDbHelper();
        dbHelper.getWritableDatabase().execSQL(VocListSql.CREATE_QUERY.replace("{name}", tableName));
        loaded = true;
    }

    public void readTable()
    {
        if (loaded)
            return;

        VocDbHelper dbHelper = MainActivity.getDbHelper();
        final SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(
                tableName,
                new String[]{VocListSql.COLUMN_NAME_LANGUAGE_1, VocListSql.COLUMN_NAME_LANGUAGE_2},
                null,
                null,
                null,
                null,
                VocListSql._ID + " ASC"

        );

        if (cursor.moveToFirst()) {
            do {
                String word1, word2;

                word1 = cursor.getString(cursor.getColumnIndex(VocListSql.COLUMN_NAME_LANGUAGE_1));
                word2 = cursor.getString(cursor.getColumnIndex(VocListSql.COLUMN_NAME_LANGUAGE_2));

                words1.add(word1);
                words2.add(word2);

                // Log.d("List " + name, word1 + "\t|" + word2);
            } while (cursor.moveToNext());
        }

        cursor.close();

        loaded = true;
    }

    public void saveTable()
    {
        VocDbHelper dbHelper = MainActivity.getDbHelper();
        final SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(tableName, null, null);

        for (int i = 0; i < words1.size(); i++)
        {
            String word1 = words1.get(i);
            String word2 = words2.get(i);

            ContentValues values = new ContentValues();
            values.put(VocListSql.COLUMN_NAME_LANGUAGE_1, word1);
            values.put(VocListSql.COLUMN_NAME_LANGUAGE_2, word2);

            db.insert(tableName, null, values);
        }
    }

    public boolean isLoaded()
    {
        return loaded;
    }

    public String getName()
    {
        return name;
    }

    public String getFirstLanguage()
    {
        return language1Name;
    }

    public String getSecondLanguage()
    {
        return language2Name;
    }

    private class VocListSql implements BaseColumns
    {
        public static final String COLUMN_NAME_LANGUAGE_1 = "language_1";
        public static final String COLUMN_NAME_LANGUAGE_2 = "language_2";

        public static final String TYPE_TEXT = "TEXT";

        public static final String CREATE_QUERY = "CREATE TABLE \"{name}\" "
                + "(" + _ID + " INTEGER AUTO_INCREMENT PRIMARY KEY, "
                + COLUMN_NAME_LANGUAGE_1 + " " + TYPE_TEXT + ", "
                + COLUMN_NAME_LANGUAGE_2 + " " + TYPE_TEXT + ")";
    }

}
